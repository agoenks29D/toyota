<?php
$data 			= Timber::get_context();
$data['posts'] 	= Timber::get_posts();
$data['post'] 	= new Timber\Post();
$data['menu'] 	= new Timber\Menu();
$data['cars']	= Timber::get_posts(array('category_name' => 'car'));
if(is_home())
{
	Timber::render('views/home.html', $data);
}
else if(is_single())
{
	Timber::render('views/single.html', $data);
}
else if(is_category())
{
	Timber::render('views/category.html', $data);
}
else if(is_page())
{
	Timber::render('views/page.html', $data);
}
else
{
	Timber::render('views/404.html', $data);
}