<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once(__DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php'); // autoload vendor package
$timber = new Timber\Timber();

function init_setup()
{
	// add_theme_support('custom-logo');
	add_theme_support('post-thumbnails',['post','page']);
	add_theme_support('html5',['comment-list','comment-form','search-form','gallery','caption']);
	add_theme_support('post-formats',['aside','gallery','link','image','quote','video']);
}
add_action('after_setup_theme','init_setup');


function admin_notice_activated_theme(){
    global $pagenow;
    if (is_admin() && ($pagenow == 'themes.php') && isset( $_GET['activated'] ))
    {
    	echo '
		    <div class="notice notice-success is-dismissible">
				<div class="notice-content">
					<p>Thank you for installing Car Service !!</p>
					<p>Visit Theme Developer <a target="_blank" href="https://www.agungdirgantara.id">Site</a> </p>
				</div>
			</div>
    	';		
	}
}
add_action('admin_notices', 'admin_notice_activated_theme');

Routes::map('send_mail/:module?', function($params){
	$mail = new PHPMailer(true);
	try
	{
	    //Server settings
	    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'srv61.niagahoster.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'sales@info-toyotamedan.id';                 // SMTP username
	   	$mail->Password = 'qS%G0;yHy88U';                           // SMTP password
	    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 465;                                    // TCP port to connect to

	    // Set Mail From
	    $mail->setFrom('sales@info-toyotamedan.id', 'Sales');

	    //Recipients
	    $mail->addAddress('jansen.toyotamedanid@yahoo.com', 'Jansen'); // Add a recipient
	    $mail->addAddress('agungmasda29@gmail.com', 'Agung Dirgantara (Web Developer)'); // Add a recipient

		switch ($params['module'])
		{
			case 'booking_service':
				$data['title'] 		= '<h1> Booking Service</h1>';
				$data['content']	= '<table class="table table-bordered">';
				$data['content'] 	.= '<tr><td>Nama </td><td>: '.$_POST['nama'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Telepon </td><td>: '.$_POST['telp'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Plat </td><td>: '.$_POST['plat'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Tanggal </td><td>: '.$_POST['tanggal'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Jam </td><td>: '.$_POST['jam'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Jenis </td><td>: '.$_POST['jenis'].'</td></tr>';
				$data['content'] 	.= '</table>';
				$body = Timber::fetch('views/mail_template.html', $data);
				$mail->isHTML(true);
			    $mail->Subject = 'Booking Service';
			    $mail->Body    = $body;
			    $mail->AltBody = 'Booking Service : ';
			break;

			case 'info_promo':
				$data['title'] 		= '<h1> Request Promo Info</h1>';
				$data['content']	= '<table class="table table-bordered">';
				$data['content'] 	.= '<tr><td>Nama </td><td>: '.$_POST['nama'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Telepon </td><td>: '.$_POST['telp'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Mobil </td><td>: '.$_POST['mobil'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Pembelian </td><td>: '.$_POST['pembelian'].'</td></tr>';
				$data['content'] 	.= '</table>';
				$body = Timber::fetch('views/mail_template.html', $data);
				$mail->isHTML(true);
			    $mail->Subject = 'Request Promo Info';
			    $mail->Body    = $body;
			    $mail->AltBody = 'Request Promo Info : ';
			break;
			
			// default will be contact
			default:
				$data['title'] 		= '<h1> Contact Admin</h1>';
				$data['content']	= '<table class="table table-bordered">';
				$data['content'] 	.= '<tr><td>Nama </td><td>: '.$_POST['name'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Email </td><td>: '.$_POST['email'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Subject </td><td>: '.$_POST['subject'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Phone </td><td>: '.$_POST['phone'].'</td></tr>';
				$data['content'] 	.= '<tr><td>Message </td><td>: '.$_POST['message'].'</td></tr>';
				$data['content'] 	.= '</table>';
				$body = Timber::fetch('views/mail_template.html', $data);
				$mail->isHTML(true);
			    $mail->Subject = 'Contact Admin';
			    $mail->Body    = $body;
			    $mail->AltBody = 'Contact Admin : ';
			break;
		}

		if($mail->send())
	    {
	    	wp_send_json(['status' => 'success','data' => $_POST]);
	    }
	    else {
	    	wp_send_json(['status' => 'failed']);
	    }
	}
	catch (Exception $e)
	{
		wp_send_json(['status' => 'failed','message' => $mail->ErrorInfo]);
	}
});
